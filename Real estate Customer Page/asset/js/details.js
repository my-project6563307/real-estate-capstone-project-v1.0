$(document).ready(function(){
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

        var gUrl = new URL(window.location.href);

        var gId = gUrl.searchParams.get("id");

        var gRealEsateUrl = "http://localhost:8080/real_estates";
    
        var gCustomerUrL = "http://localhost:8080/customers";
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
        onPageLoading();

        $("#btn-add-customer").on("click", function(){
            onBtnAddCustomerClick();
        })
    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
        function onPageLoading(){
            "use strict"
            callApiGetRealEstateById(gId);
        }
        
        // Hàm xử lí nhấn nút add customer
        function onBtnAddCustomerClick(){
            "use strict"
            var vCustomerInfo = {
                contactName: "",
                email: "",
                mobile: "",
                address: "",
                contactTitle: "",
                note: ""
            }

            // Read data customer form 
            readDataCustomerForm(vCustomerInfo);
            // Gọi api thêm customer vào database
            callApiAddCustomer(vCustomerInfo);

        }
        // Hàm gọi api lấy real estate
        function callApiGetRealEstateById(paramId){
            "use strict"
            $.ajax({
                url: gRealEsateUrl  + "/" + paramId,
                type: "GET",
                dataType: "json",
                async: false,
                success: function(res){
                    console.log(res);
                    loadRealEstateToContent(res);
                },
                error: function(er){
                    console.log(er.responseText);
                }
            })
        }
        // Hàm gọi api add customer
        function callApiAddCustomer(paramObj){
            "use strict"
            var vJsonCustomerObj = JSON.stringify(paramObj);
            $.ajax({
                url: gCustomerUrL,
                type: "POST",
                contentType: "application/json",
                data: vJsonCustomerObj,
                success: function(res){
                    console.log(res);
                    alert("SUCCESS");
                },
                error: function(er){
                    console.log(er.responseText);
                }
            })
        }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
    function loadRealEstateToContent(paramRes){
        "use strict"
        var vContentWrap = $("#content-realestate");
        vContentWrap.empty();
        var vContent = `
        <img src="` + paramRes.photo + `" alt="" class="estate-img">
        <div class="list">
            <h2 class="title">` + paramRes.title + `</h2>
            <p class="desc">Apartment Code: ` + paramRes.apart_code + `</p>
            <p class="desc">Acreage: ` + paramRes.acreage + `</p>
            <p class="desc">Numbers of bedrooms: ` + paramRes.bedroom + `</p>
            <p class="desc">Numbers of baths : ` + paramRes.bath + `</p>
            <p class="desc">Numbers of floors: ` + paramRes.number_floors + `</p>
        </div>`
        
        vContentWrap.append(vContent);

        var vContentProjectWrap = $("#content-project");
        vContentProjectWrap.empty();
        var vContentProject = `<h2 class="title">In Project: `+ paramRes.project.name +`</h2>
        <p class="address">` + paramRes.project.address  +", " + paramRes.project.ward.name + ", " + paramRes.project.province.name + `</p>
        <p class="desc">`+ paramRes.project.description +`</p>
        <div class="body">
            <img src="`+ paramRes.project.photo +`" alt="" class="project-img">
            <ul class="project-feature">
                <h2 class="sub-title">Features:</h2>
                <li><p class="desc"> <b>Acreage: </b>`+ paramRes.project.acreage +`</p></li>
                <li><p class="desc"> <b>Number apartment: </b>`+ paramRes.project.numApartment +`</p></li>
                <li><p class="desc"> <b>Number block: </b>`+ paramRes.project.numBlock +`</p></li>
                <li><p class="desc"> <b>Number floors: </b>`+ paramRes.project.numFloors +`</p></li>
                <li><p class="desc"> <b>Design unit: </b>`+ paramRes.project.designUnit.name +`</p></li>
                <li><p class="desc"> <b>Investor: </b>`+ paramRes.project.investor.name +`</p></li>
            </ul>
        </div>`
        
        vContentProjectWrap.append(vContentProject);
    }

    // Hàm thu thập dữ liệu
    function readDataCustomerForm(paramObj){
        "use strict"
        paramObj.contactName = $("#inp-name").val();
        paramObj.email = $("#inp-email").val();
        paramObj.mobile = $("#inp-mobile").val();
        paramObj.address = $("#inp-address").val();
        paramObj.contactTitle = $("#inp-title").val();
        paramObj.note = $("#inp-note").val();
    }
})