$(document).ready(function () {
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const vtoken = getCookie("token");
    var gInitialDataTable = {
            paging: true,
            lengthChange: false,
            searching: true,
            ordering: true,
            info: true,
            autoWidth: true,
            responsive: false,
            columns: "",
            columnDefs: "",
    }
    var gAddressMapUrl = "http://localhost:8080/address_map";
    var gConstructionUrl = "http://localhost:8080/contractors";
    var gCustomerUrl = "http://localhost:8080/customers";
    var gDesignUnitUrl = "http://localhost:8080/design_units";
    var gEmployeeUrl = "http://localhost:8080/employees";
    var gInvestorUrl = "http://localhost:8080/investors";
    var gLocationUrl = "http://localhost:8080/locations";
    var gMasterLayoutUrl = "http://localhost:8080/master_layouts";
    var gProjectUrl = "http://localhost:8080/projects";
    var gRealEsateUrl = "http://localhost:8080/real_estates";
    var gRegionLinkUrl = "http://localhost:8080/region_links";
    var gUtilitiesUrl = "http://localhost:8080/utilities";
    var gProvinceUrl = "http://localhost:8080/provinces";
    var gDistrictUrl = "http://localhost:8080/districts";
    var gWardUrl = "http://localhost:8080/wards";
    var gStreetUrl = "http://localhost:8080/streets";

    var gSavedDataTable = "";

    var gRowSelectedId = "";

    var gUrl = new URL(window.location.href);
    
    var gId = gUrl.searchParams.get("id");

    var urlInfo = "http://localhost:8080/employees/me";
    
    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */

    //Set sự kiện cho nút logout
    $("#btn-logout").on("click", function() {
        redirectToLogin();
    });

    // Tạo sự kiện cho bảng address map
    $(document).on("click", "#address_map", function(){
        console.log("address-map");

        
        createContentAddressTable();
        
        var vTableAddress = createAddressMapDataTable();
        
        gSavedDataTable.destroy();
        gSavedDataTable = vTableAddress;
        
        callApiGetAddressMap(vTableAddress);
        changeActiveAction("address-map");
    })

    // Tạo sự kiện cho bảng construction contractor
    $(document).on("click", "#contractors", function(){
        console.log("contructions");

        
        createContentContractorTable();
        
        var vContractorTable = createContractorDataTable();
        
        gSavedDataTable.destroy();
        gSavedDataTable = vContractorTable;
        
        callApiGetConstruction(vContractorTable);
        changeActiveAction("contractors");
    })

    // Tạo sự kiện cho bảng customer
    $(document).on("click", "#customers", function(){
        console.log("customers");

        changeActiveAction("customers");

        createContentCustomerTable();

        var vCustomerTable = createCustomerDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vCustomerTable;

        callApiGetCustomers(vCustomerTable);
    })

    // Tạo sự kiện cho bảng design unit
    $(document).on("click", "#design_units", function(){
        console.log("design unit");

        changeActiveAction("design_units");

        createContentDesignUnitTable();

        var vDesignUnitTable = createDesignUnitDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vDesignUnitTable;

        callApiGetDesignUnits(vDesignUnitTable);
    })

    // Tạo sự kiện cho bảng employee
    $(document).on("click", "#employees", function(){
        console.log("employee");

        changeActiveAction("employees");

        createContentEmployeeTable();

        var vEmloyeeTable = createEmployeeDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vEmloyeeTable;

        callApiGetEmployees(vEmloyeeTable);

    })
    $(document).on("click", "#investors", function(){
        console.log("Investors");

        changeActiveAction("investors");

        createContentInvestorTable();

        var vInvestorTable = createInvestorDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vInvestorTable;

        callApiGetInvestors(vInvestorTable);

    })
    $(document).on("click", "#locations", function(){
        console.log("Locations");

        changeActiveAction("locations");

        createContentLocationTable();

        var vLocationTable = createLocationDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vLocationTable;

        
        callApiGetLocations(vLocationTable);
    })
    $(document).on("click", "#master_layouts", function(){
        console.log("Master Layout");

        changeActiveAction("master_layouts");

        createContentMasterLayoutTable();

        var vMasterLayoutTable = createMasterLayoutDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vMasterLayoutTable;

        callApiGetMasterLayouts(vMasterLayoutTable);
    })
    $(document).on("click", "#projects", function(){
        console.log("Project");

        changeActiveAction("projects");

        createContentProjectTable();

        var vProjectTable = createProjectDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vProjectTable;

        callApiGetProjects(vProjectTable);

    })
    $(document).on("click", "#real_estates", function(){
        console.log("Real estate");

        changeActiveAction("real_estates");

        createContentRealEstateTable();

        var vRealEstateTable = createRealEstateDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vRealEstateTable;

        callApiGetRealEstates(vRealEstateTable);
    })
    $(document).on("click", "#region_links", function(){
        console.log("region link");

        changeActiveAction("region_links");

        createContentRegionLinkTable();

        var vRegionLinkTable = createRegionLinkDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vRegionLinkTable;

        callApiGetRegionLinks(vRegionLinkTable);
    })
    $(document).on("click", "#provinces", function(){
        console.log("Province");

        changeActiveAction("provinces");

        createContentProvinceTable();

        var vProvinceTable = createProvinceDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vProvinceTable;

        callApiGetProvince(vProvinceTable);

    })
    $(document).on("click", "#districts", function(){
        console.log("district");

        changeActiveAction("districts");

        createContentDistrictTable();

        var vDistrictTable = createDistrictDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vDistrictTable;

        callApiGetDistrict(vDistrictTable);
    })
    $(document).on("click", "#wards", function(){
        console.log("Ward");

        changeActiveAction("wards");

        createContentWardTable();

        var vWardTable = createWardDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vWardTable;

        callApiGetWard(vWardTable);
    })
    $(document).on("click", "#streets", function(){
        console.log("Street");

        changeActiveAction("streets");

        createContentStreetTable();

        var vStreetTable = createStreetDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vStreetTable;

        callApiGetStreet(vStreetTable);
    })
    $(document).on("click", "#utilities", function(){
        console.log("Utilities");

        changeActiveAction("utilities");

        createContentUtilitiesTable();

        var vUtilitiesTable = createUtilitiesDataTable();

        gSavedDataTable.destroy();
        gSavedDataTable = vUtilitiesTable;

        callApiGetUtilities(vUtilitiesTable);
    })

    // Khởi tạo sự kiện nhấn icon edit
    $(document).on("click", "#edit-address-map", function(){
        onRowClick(this);
        window.location.href = "./subpage/address-map-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-contractor", function(){
        onRowClick(this);
        window.location.href = "./subpage/contractor-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-customer", function(){
        onRowClick(this);
        window.location.href = "./subpage/customer-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-design-unit", function(){
        onRowClick(this);
        window.location.href = "./subpage/design-unit-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-employee", function(){
        onRowClick(this);
        window.location.href = "./subpage/employee-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-investor", function(){
        onRowClick(this);
        window.location.href = "./subpage/investor-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-location", function(){
        onRowClick(this);
        window.location.href = "./subpage/location-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-master-layout", function(){
        onRowClick(this);
        window.location.href = "./subpage/master-layout-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-project", function(){
        onRowClick(this);
        window.location.href = "./subpage/project-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-real-estate", function(){
        onRowClick(this);
        window.location.href = "./subpage/real-estate-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-region-link", function(){
        onRowClick(this);
        window.location.href = "./subpage/region-link-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-province", function(){
        onRowClick(this);
        window.location.href = "./subpage/province-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-district", function(){
        onRowClick(this);
        window.location.href = "./subpage/district-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-ward", function(){
        onRowClick(this);
        window.location.href = "./subpage/ward-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-street", function(){
        onRowClick(this);
        window.location.href = "./subpage/street-edit.html?id=" + gRowSelectedId;
    })
    $(document).on("click", "#edit-utilities", function(){
        onRowClick(this);
        window.location.href = "./subpage/utilities-edit.html?id=" + gRowSelectedId;
    })

    // Khởi tạo sự kiện nhấn nút add
    $(document).on("click", "#btn-address-map", function(){
        window.location.href = "./subpage/address-map-add.html";
    })
    $(document).on("click", "#btn-contractor", function(){
        window.location.href = "./subpage/contractor-add.html";
    })
    $(document).on("click", "#btn-customer", function(){
        window.location.href = "./subpage/customer-add.html";
    })
    $(document).on("click", "#btn-design-unit", function(){
        window.location.href = "./subpage/design-unit-add.html";
    })
    $(document).on("click", "#btn-employee", function(){
        window.location.href = "./subpage/employee-add.html";
    })
    $(document).on("click", "#btn-investor", function(){
        window.location.href = "./subpage/investor-add.html";
    })
    $(document).on("click", "#btn-location", function(){
        window.location.href = "./subpage/location-add.html";
    })
    $(document).on("click", "#btn-master-layout", function(){
        window.location.href = "./subpage/master-layout-add.html";
    })
    $(document).on("click", "#btn-project", function(){
        window.location.href = "./subpage/project-add.html";
    })
    $(document).on("click", "#btn-real-estate", function(){
        window.location.href = "./subpage/real-estate-add.html";
    })
    $(document).on("click", "#btn-region-link", function(){
        window.location.href = "./subpage/region-link-add.html";
    })
    $(document).on("click", "#btn-province", function(){
        window.location.href = "./subpage/province-add.html";
    })
    $(document).on("click", "#btn-district", function(){
        window.location.href = "./subpage/district-add.html";
    })
    $(document).on("click", "#btn-ward", function(){
        window.location.href = "./subpage/ward-add.html";
    })
    $(document).on("click", "#btn-street", function(){
        window.location.href = "./subpage/street-add.html";
    })
    $(document).on("click", "#btn-utilities", function(){
        window.location.href = "./subpage/utilities-add.html";
    })
    // Khởi tạo sự kiện nhấn icon delete
    $(document).on("click", "#delete-address-map", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-address-map")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-contractor", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-contractor")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-customer", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-customer")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-design-unit", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-design-unit")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-employee", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-employee")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-investor", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-investor")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-location", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-location")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-master-layout", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-master-layout")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-project", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-project")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-real-estate", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-realestate")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-region-link", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-region-link")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-province", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-province")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-district", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-district")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-ward", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-ward")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-street", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-street")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })
    $(document).on("click", "#delete-utilities", function(){
        $(".btn-confirm-delete").prop("id", "btn-delete-utilities")
        $("#delete-modal").modal("show");
        onRowClick(this);
    })

    // Sự kiện nhấn nút xác nhận xoá
    $(document).on("click", "#btn-delete-address-map", function(){
        callApiDeleteRowById("address_map");
    })
    $(document).on("click", "#btn-delete-contractor", function(){
        callApiDeleteRowById("contractors");
    })
    $(document).on("click", "#btn-delete-customer", function(){
        callApiDeleteRowById("customers");
    })
    $(document).on("click", "#btn-delete-design-unit", function(){
        callApiDeleteRowById("design_units");
    })
    $(document).on("click", "#btn-delete-employee", function(){
        callApiDeleteRowById("employees");
    })
    $(document).on("click", "#btn-delete-investor", function(){
        callApiDeleteRowById("investors");
    })
    $(document).on("click", "#btn-delete-location", function(){
        callApiDeleteRowById("locations");
    })
    $(document).on("click", "#btn-delete-master-layout", function(){
        callApiDeleteRowById("master_layouts");
    })
    $(document).on("click", "#btn-delete-project", function(){
        callApiDeleteRowById("projects");
    })
    $(document).on("click", "#btn-delete-real-estate", function(){
        callApiDeleteRowById("real_estates");
    })
    $(document).on("click", "#btn-delete-region-link", function(){
        callApiDeleteRowById("region_links");
    })
    $(document).on("click", "#btn-delete-province", function(){
        callApiDeleteRowById("provinces");
    })
    $(document).on("click", "#btn-delete-district", function(){
        callApiDeleteRowById("districts");
    })
    $(document).on("click", "#btn-delete-ward", function(){
        callApiDeleteRowById("wards");
    })
    $(document).on("click", "#btn-delete-street", function(){
        callApiDeleteRowById("streets");
    })
    $(document).on("click", "#btn-delete-utilities", function(){
        callApiDeleteRowById("utilities");
    })

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    function onPageLoading() {
        "use strict";
        console.log("abc");
            callApiCheckUser();

            var vTableAddress = createAddressMapDataTable();
        
            gSavedDataTable = vTableAddress;
    
            callApiGetAddressMap(vTableAddress);
            if (gId == 1){
                $("#address_map").click();
            }
            else if(gId == 2){
                $("#contractors").click();
            }
            else if(gId == 3){
                $("#customers").click();
            }
            else if(gId == 4){
                $("#design_units").click();
            }
            else if(gId == 5){
                $("#employees").click();
            }
            else if(gId == 6){
                $("#investors").click();
            }
            else if(gId == 7){
                $("#locations").click();
            }
            else if(gId == 8){
                $("#master_layouts").click();
            }
            else if(gId == 9){
                $("#projects").click();
            }
            else if(gId == 10){
                $("#real_estates").click();
            }
            else if(gId == 11){
                $("#region_links").click();
            }
            else if(gId == 12){
                $("#provinces").click();
            }
            else if(gId == 13){
                $("#districts").click();
            }
            else if(gId == 14){
                $("#wards").click();
            }
            else if(gId == 15){
                $("#streets").click();
            }
            else if(gId == 16){
                $("#utilities").click();
            }
    }

    // Hàm xử lí khi click vào row table
    function onRowClick(paramIcon){
        "use trict"
        var vRow = $(paramIcon).closest("tr");
        gRowSelectedId = "";
        var vDataOnRow = gSavedDataTable.row(vRow).data();
        gDataSelectedRow = vDataOnRow;
        gRowSelectedId = vDataOnRow.id;
        console.log(gRowSelectedId);
    }

    function callApiCheckUser(){
        $.ajax({
            url: urlInfo + "?token=" + vtoken,
            type: "GET",
            dataType: "json",
            success: function(responseObject) {
                console.log(responseObject);
                // displayUser(responseObject);
            },
            error: function(xhr) {
                console.log(xhr);
                // Khi token hết hạn, AJAX sẽ trả về lỗi khi đó sẽ redirect về trang login để người dùng đăng nhập lại
                redirectToLogin()
            }
        });
    }
    // Hàm gọi Api xoá row
    function callApiDeleteRowById(paramTableName){
        $.ajax({
            url: "http://localhost:8080/"+ paramTableName + "/" + gRowSelectedId,
            type: "DELETE",
            dataType: "json",
            success: function (res) {
                console.log(res);
                window.location.href = window.location.href;
                $("#" + paramTableName + "").click();
            },
            error: function (err) {
                console.log(err.responseText);
                $("#delete-modal").modal("hide");
                alert("Fail to delete data!");
            }
        })
    }
    //Hàm gọi api lấy address map
    function callApiGetAddressMap(paramTable) {
        "use strict";
        $.ajax({
            url: gAddressMapUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("address-map");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get address_map!");
            },
        });
        
    }
    //Hàm gọi api lấy construction
    function callApiGetConstruction(paramTable) {
        "use strict";
        $.ajax({
            url: gConstructionUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("contractors");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get contractors!");
            },
        });
        
    }
    //Hàm gọi api lấy customer
    function callApiGetCustomers(paramTable) {
        "use strict";
        $.ajax({
            url: gCustomerUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("customers");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get customer!");
            },
        });
    }
    //Hàm gọi api lấy design unit
    function callApiGetDesignUnits(paramTable) {
        "use strict";
        $.ajax({
            url: gDesignUnitUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("design_units");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get design unit!");
            },
        });
    }

    //Hàm gọi api lấy employee
    function callApiGetEmployees(paramTable) {
        "use strict";
        $.ajax({
            url: gEmployeeUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("employees");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get employees!");
            },
        });
    }

    //Hàm gọi api lấy Investor
    function callApiGetInvestors(paramTable) {
        "use strict";
        $.ajax({
            url: gInvestorUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("investors");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Investor!");
            },
        });
    }
    //Hàm gọi api lấy Location
    function callApiGetLocations(paramTable) {
        "use strict";
        $.ajax({
            url: gLocationUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("locations");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Location!");
            },
        });
    }
    //Hàm gọi api lấy Master layout
    function callApiGetMasterLayouts(paramTable) {
        "use strict";
        $.ajax({
            url: gMasterLayoutUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("master_layouts");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Master layout!");
            },
        });
    }
    //Hàm gọi api lấy Projects
    function callApiGetProjects(paramTable) {
        "use strict";
        $.ajax({
            url: gProjectUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("projects");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Projects!");
            },
        });
    }
    //Hàm gọi api lấy Real estate
    function callApiGetRealEstates(paramTable) {
        "use strict";
        $.ajax({
            url: gRealEsateUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Real estate!");
                changeActiveAction("real_estates");
            },
        });
    }
    //Hàm gọi api lấy Region link
    function callApiGetRegionLinks(paramTable) {
        "use strict";
        $.ajax({
            url: gRegionLinkUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("region_links");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Region link!");
            },
        });
    }
    //Hàm gọi api lấy Utilities
    function callApiGetUtilities(paramTable) {
        "use strict";
        $.ajax({
            url: gUtilitiesUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("utilities");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Utilities!");
            },
        });
    }

    //Hàm gọi api lấy provine
    function callApiGetProvince(paramTable) {
        "use strict";
        $.ajax({
            url: gProvinceUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("provinces");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Province!");
            },
        });
    }
    //Hàm gọi api lấy district
    function callApiGetDistrict(paramTable) {
        "use strict";
        $.ajax({
            url: gDistrictUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("districts");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get District!");
            },
        });
    }
    //Hàm gọi api lấy ward
    function callApiGetWard(paramTable) {
        "use strict";
        $.ajax({
            url: gWardUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("wards");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Wards!");
            },
        });
    }
    //Hàm gọi api lấy street
    function callApiGetStreet(paramTable) {
        "use strict";
        $.ajax({
            url: gStreetUrl,
            type: "GET",
            dataType: "json",
            success: function (res) {
                console.log(res);
                loadDataToTable(res, paramTable);
                changeActiveAction("streets");
            },
            error: function (err) {
                console.log(err.responseText);
                console.log("Fail to get Street!");
            },
        });
    }
    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

    //Hàm get Cookie đã giới thiệu ở bài trước
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    //Hàm logout
    function redirectToLogin() {
        // Trước khi logout cần xóa token đã lưu trong cookie
        setCookie("token", "", 1);
        window.location.href = "./login.html";
    }

    //Hàm setCookie đã giới thiệu ở bài trước
    function setCookie(cname, cvalue) {
        document.cookie = cname + "=" + cvalue + ";" + ";path=/";
    }
    //Load dữ liệu vào bảng
    function loadDataToTable(paramArr, paramTable) {
        "use strict";
        paramTable.clear();
        paramTable.rows.add(paramArr);
        paramTable.draw();
    }

    // Hàm change active 
    function changeActiveAction(paramName){
        "use strict"
        $(".nav-link").attr("class","nav-link");
        $("#"+ paramName +"").attr("class","nav-link active");
    }
    // Tạo content address map
    function createContentAddressTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-address-map");
        $("#title-table").html("Address Map");
        var vFrameTable = `
        <table
            id="table-address"
            class="table table-bordered table-hover">
            <thead id="table-body">
                <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Address</th>
                    <th>Lat</th>
                    <th>Lng</th>
                </tr>
            </thead>
        </table>`

        $("#table-content").html(vFrameTable);
    }
    // Tạo data table bảng address map
    function createAddressMapDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "address" },
            { data: "lat" },
            { data: "lng" },
            
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-address-map" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-address-map" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        console.log(gInitialDataTable);
        var vTable = $("#table-address").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content contractor table
    function createContentContractorTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-contractor");
        $("#title-table").html("Construction Contractor");
        var vFrameTable = `
            <table
                id="table-contractor"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                        <th>Action</th>
                        <th>Id</th>
                        <th>address</th>
                        <th>description</th>
                        <th>email</th>
                        <th>fax</th>
                        <th>name</th>
                        <th>note</th>
                        <th>phone</th>
                        <th>phone2</th>
                        <th>projects</th>
                        <th>website</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng contractor map
    function createContractorDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "address" },
            { data: "description" },
            { data: "email" },
            { data: "fax" },
            { data: "name" },
            { data: "note" },
            { data: "phone" },
            { data: "phone2" },
            { data: "projects" },
            { data: "website" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-contractor" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-contractor" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        console.log(gInitialDataTable);
        var vTable = $("#table-contractor").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content customer table
    function createContentCustomerTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-customer");
        $("#title-table").html("Customer");
        var vFrameTable = `
            <table
                id="table-customer"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                        <th>Action</th>
                        <th>Id</th>
                        <th>address</th>
                        <th>contactName</th>
                        <th>contactTitle</th>
                        <th>createBy</th>
                        <th>createDate</th>
                        <th>email</th>
                        <th>mobile</th>
                        <th>note</th>
                        <th>updateBy</th>
                        <th>updateDate</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng customer
    function createCustomerDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "address" },
            { data: "contactName" },
            { data: "contactTitle" },
            { data: "createBy" },
            { data: "createDate" },
            { data: "email" },
            { data: "mobile" },
            { data: "note" },
            { data: "updateBy" },
            { data: "updateDate" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-customer" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-customer" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        console.log(gInitialDataTable);
        var vTable = $("#table-customer").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content design unit table
    function createContentDesignUnitTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-design-unit");
        $("#title-table").html("Design Unit");
        var vFrameTable = `
            <table
                id="table-design-unit"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>address</th>
                    <th>description</th>
                    <th>email</th>
                    <th>fax</th>
                    <th>name</th>
                    <th>note</th>
                    <th>phone</th>
                    <th>phone2</th>
                    <th>projects</th>
                    <th>website</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng design unit
    function createDesignUnitDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "address" },
            { data: "description" },
            { data: "email" },
            { data: "fax" },
            { data: "name" },
            { data: "note" },
            { data: "phone" },
            { data: "phone2" },
            { data: "projects" },
            { data: "website" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-design-unit" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-design-unit" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        console.log(gInitialDataTable);
        var vTable = $("#table-design-unit").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content employee table
    function createContentEmployeeTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-employee");
        $("#title-table").html("Employee");
        var vFrameTable = `
            <table
                id="table-employee"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>firstName</th>
                    <th>lastName</th>
                    <th>address</th>
                    <th>birthDate</th>
                    <th>city</th>
                    <th>country</th>
                    <th>homePhone</th>
                    <th>username</th>
                    <th>password</th>
                    <th>photo</th>
                    <th>email</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng employee
    function createEmployeeDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "firstName" },
            { data: "lastName" },
            { data: "address" },
            { data: "birthDate" },
            { data: "city" },
            { data: "country" },
            { data: "homePhone" },
            { data: "username" },
            { data: "password" },
            { data: "photo" },
            { data: "email" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-employee" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-employee" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        console.log(gInitialDataTable);
        var vTable = $("#table-employee").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content investor table
    function createContentInvestorTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-investor");
        $("#title-table").html("Investor");
        var vFrameTable = `
            <table
                id="table-investor"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>address</th>
                    <th>description</th>
                    <th>email</th>
                    <th>fax</th>
                    <th>name</th>
                    <th>note</th>
                    <th>phone</th>
                    <th>phone2</th>
                    <th>projects</th>
                    <th>website</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng investor
    function createInvestorDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "address" },
            { data: "description" },
            { data: "email" },
            { data: "fax" },
            { data: "name" },
            { data: "note" },
            { data: "phone" },
            { data: "phone2" },
            { data: "projects" },
            { data: "website" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-investor" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-investor" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        console.log(gInitialDataTable);
        var vTable = $("#table-investor").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content location table
    function createContentLocationTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-location");
        $("#title-table").html("Locations");
        var vFrameTable = `
            <table
                id="table-location"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng location
    function createLocationDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "latitude" },
            { data: "longitude" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-location" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-location" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        console.log(gInitialDataTable);
        var vTable = $("#table-location").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content Master layout table
    function createContentMasterLayoutTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-master-layout");
        $("#title-table").html("Master Layout");
        var vFrameTable = `
            <table
                id="table-master-layout"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Project</th>
                    <th>Acreage</th>
                    <th>Apartment List</th>
                    <th>Description</th>
                    <th>Photo</th>
                    <th>Create Date</th>
                    <th>Update Date</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng Master layout
    function createMasterLayoutDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "name" },
            { data: "project.name" },
            { data: "acreage" },
            { data: "apartmentList" },
            { data: "description" },
            { data: "photo" },
            { data: "date_create" },
            { data: "date_update" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-master-layout" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-master-layout" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        console.log(gInitialDataTable);
        var vTable = $("#table-master-layout").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content Project table
    function createContentProjectTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-project");
        $("#title-table").html("Projects");
        var vFrameTable = `
            <table
                id="table-project"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Acreage</th>
                    <th>Province</th>
                    <th>District</th>
                    <th>Ward</th>
                    <th>Num Apartment</th>
                    <th>Num Block</th>
                    <th>Num Floors</th>
                    <th>Contractor</th>
                    <th>Design Unit</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng Project
    function createProjectDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "name" },
            { data: "address" },
            { data: "acreage" },
            { data: "province.name" },
            { data: "district.name" },
            { data: "ward.name" },
            { data: "numApartment" },
            { data: "numBlock" },
            { data: "numFloors" },
            { data: "constructionContractor.name" },
            { data: "designUnit.name" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-project" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-project" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        console.log(gInitialDataTable);
        var vTable = $("#table-project").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content Real estate table
    function createContentRealEstateTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-real-estate");
        $("#title-table").html("Projects");
        var vFrameTable = `
            <table
                id="table-real-estate"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Type</th>
                    <th>Address</th>
                    <th>Acreage</th>
                    <th>Province</th>
                    <th>District</th>
                    <th>Ward</th>
                    <th>Request</th>
                    <th>Customer</th>
                    <th>Price</th>
                    <th>date_create</th>
                    <th>Direction</th>
                    <th>Bedroom</th>
                    <th>Furniture Type</th>
                    <th>Price Rent</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng Real estate
    function createRealEstateDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "type" },
            { data: "address" },
            { data: "acreage" },
            { data: "province.name" },
            { data: "district.name" },
            { data: "ward.name" },
            { data: "request" },
            { data: "customer.contactName" },
            { data: "price" },
            { data: "date_create" },
            { data: "direction" },
            { data: "bedroom" },
            { data: "furniture_type" },
            { data: "price_rent" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-real-estate" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-real-estate" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        var vTable = $("#table-real-estate").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content Region link table
    function createContentRegionLinkTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-region-link");
        $("#title-table").html("Region Links");
        var vFrameTable = `
            <table
                id="table-region-link"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Description</th>
                    <th>Photo</th>
                    <th>Lat</th>
                    <th>Lng</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng Region link
    function createRegionLinkDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "name" },
            { data: "address" },
            { data: "description" },
            { data: "photo" },
            { data: "lat" },
            { data: "lng" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-region-link" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-region-link" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        var vTable = $("#table-region-link").DataTable(gInitialDataTable);
        return vTable;
    }
    // Tạo Content Utilities table
    function createContentUtilitiesTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-utilities");
        $("#title-table").html("Utilities");
        var vFrameTable = `
            <table
                id="table-utilities"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Photo</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng Utilities
    function createUtilitiesDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "name" },
            { data: "description" },
            { data: "photo" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-utilities" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-utilities" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        var vTable = $("#table-utilities").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content province table
    function createContentProvinceTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-province");
        $("#title-table").html("Provinces");
        var vFrameTable = `
            <table
                id="table-province"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Code</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng province
    function createProvinceDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "name" },
            { data: "code" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-province" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-province" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        var vTable = $("#table-province").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content district table
    function createContentDistrictTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-district");
        $("#title-table").html("Districts");
        var vFrameTable = `
            <table
                id="table-district"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Prefix</th>
                    <th>Province</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng district
    function createDistrictDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "name" },
            { data: "prefix" },
            { data: "province.name" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-district" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-district" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        var vTable = $("#table-district").DataTable(gInitialDataTable);
        return vTable;
    }
    // Tạo Content ward table
    function createContentWardTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-ward");
        $("#title-table").html("Wards");
        var vFrameTable = `
            <table
                id="table-ward"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Prefix</th>
                    <th>District</th>
                    <th>Province</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }
    // Tạo data table bảng ward
    function createWardDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "name" },
            { data: "prefix" },
            { data: "district.name" },
            { data: "province.name" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-ward" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-ward" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        var vTable = $("#table-ward").DataTable(gInitialDataTable);
        return vTable;
    }

    // Tạo Content street table
    function createContentStreetTable(){
        "use strict"
        $(".btn-add").prop("id", "btn-street");
        $("#title-table").html("Streets");
        var vFrameTable = `
            <table
                id="table-street"
                class="table table-bordered table-hover">
                <thead id="table-body">
                    <tr>
                    <th>Action</th>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Prefix</th>
                    <th>District</th>
                    <th>Province</th>
                    </tr>
                </thead>
            </table>`
        $("#table-content").html(vFrameTable);
    }

    // Tạo data table bảng street
    function createStreetDataTable(){
        gInitialDataTable.columns = [
            {data: "action"},
            { data: "id" },
            { data: "name" },
            { data: "prefix" },
            { data: "district.name" },
            { data: "province.name" },
        ];
        gInitialDataTable.columnDefs = [
            {
            targets: 0,
            defaultContent: '<i id = "edit-street" class="fa fa-regular fa-pen-to-square edit-icon"></i> ' 
            + '<i id = "delete-street" class="fa fa-solid fa-trash-can delete-icon"></i>',
            }
          ];
        var vTable = $("#table-street").DataTable(gInitialDataTable);
        return vTable;
    }
    onPageLoading();
});
