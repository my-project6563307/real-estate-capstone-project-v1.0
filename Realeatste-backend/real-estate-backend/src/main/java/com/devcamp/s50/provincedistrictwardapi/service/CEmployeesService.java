package com.devcamp.s50.provincedistrictwardapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.provincedistrictwardapi.model.CEmployees;
import com.devcamp.s50.provincedistrictwardapi.repository.IEmployeesRepository;

@Service
public class CEmployeesService {
    @Autowired
    IEmployeesRepository employeesRepository;

    //Get all Employees
    public ResponseEntity<List<CEmployees>> getAllEmployees(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CEmployees> employeesList = new ArrayList<>();
            employeesRepository.findAll(pageWithElements).forEach(employeesList::add);

            return new ResponseEntity<>(employeesList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Get employees by Id
    public ResponseEntity<CEmployees> getEmployeesById (int id){
        try {
            Optional<CEmployees> employeesData = employeesRepository.findById(id);
            if(employeesData.isPresent()){

                CEmployees employeesFound = employeesData.get();

                return new ResponseEntity<>(employeesFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Create a new employee
    public ResponseEntity<Object> createEmployees(CEmployees pEmployees){
        try {

            CEmployees newEmployees = new CEmployees();

            newEmployees.setLastName(pEmployees.getLastName());
            newEmployees.setFirstName(pEmployees.getFirstName());
            newEmployees.setTitle(pEmployees.getTitle());
            newEmployees.setTitleOfCourtesy(pEmployees.getTitleOfCourtesy());
            newEmployees.setBirthDate(pEmployees.getBirthDate());
            newEmployees.setHireDate(pEmployees.getHireDate());
            newEmployees.setAddress(pEmployees.getAddress());
            newEmployees.setCity(pEmployees.getCity());
            newEmployees.setRegion(pEmployees.getRegion());
            newEmployees.setPostalCode(pEmployees.getPostalCode());
            newEmployees.setCountry(pEmployees.getCountry());
            newEmployees.setHomePhone(pEmployees.getHomePhone());
            newEmployees.setExtension(pEmployees.getExtension());
            newEmployees.setPhoto(pEmployees.getPhoto());
            newEmployees.setNotes(pEmployees.getNotes());
            newEmployees.setReportsTo(pEmployees.getReportsTo());
            newEmployees.setUsername(pEmployees.getUsername());
            newEmployees.setPassword(pEmployees.getPassword());
            newEmployees.setEmail(pEmployees.getEmail());
            newEmployees.setActivated(pEmployees.getActivated());
            newEmployees.setProfile(pEmployees.getProfile());
            newEmployees.setUserLevel(pEmployees.getUserLevel());

            CEmployees savedEmployees = employeesRepository.save(newEmployees);

            return new ResponseEntity<>(savedEmployees, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified Employee: " + e.getCause().getCause().getMessage());
        }
    }

    //Update employee by id
    public ResponseEntity<Object> updateEmployeesById(int id, CEmployees pEmployees){
        try {
            Optional<CEmployees> employeesData = employeesRepository.findById(id);

            if (employeesData.isPresent()){

                CEmployees employeesFound =  employeesData.get();

                employeesFound.setLastName(pEmployees.getLastName());
                employeesFound.setFirstName(pEmployees.getFirstName());
                employeesFound.setTitle(pEmployees.getTitle());
                employeesFound.setTitleOfCourtesy(pEmployees.getTitleOfCourtesy());
                employeesFound.setBirthDate(pEmployees.getBirthDate());
                employeesFound.setHireDate(pEmployees.getHireDate());
                employeesFound.setAddress(pEmployees.getAddress());
                employeesFound.setCity(pEmployees.getCity());
                employeesFound.setRegion(pEmployees.getRegion());
                employeesFound.setPostalCode(pEmployees.getPostalCode());
                employeesFound.setCountry(pEmployees.getCountry());
                employeesFound.setHomePhone(pEmployees.getHomePhone());
                employeesFound.setExtension(pEmployees.getExtension());
                employeesFound.setPhoto(pEmployees.getPhoto());
                employeesFound.setNotes(pEmployees.getNotes());
                employeesFound.setReportsTo(pEmployees.getReportsTo());
                employeesFound.setUsername(pEmployees.getUsername());
                employeesFound.setPassword(pEmployees.getPassword());
                employeesFound.setEmail(pEmployees.getEmail());
                employeesFound.setActivated(pEmployees.getActivated());
                employeesFound.setProfile(pEmployees.getProfile());
                employeesFound.setUserLevel(pEmployees.getUserLevel());

                return new ResponseEntity<>(employeesRepository.save(employeesFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    //delete design Unit by id
    public ResponseEntity<Object> deleteEmployeesById(int id){
        try {
            Optional<CEmployees> employeesData = employeesRepository.findById(id);
            if(employeesData.isPresent()){
                employeesRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
