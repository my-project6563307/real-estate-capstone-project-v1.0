package com.devcamp.s50.provincedistrictwardapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.provincedistrictwardapi.model.CDistrict;
import com.devcamp.s50.provincedistrictwardapi.model.CProvince;
import com.devcamp.s50.provincedistrictwardapi.model.CWard;
import com.devcamp.s50.provincedistrictwardapi.repository.IDistrictRepository;
import com.devcamp.s50.provincedistrictwardapi.repository.IProvinceRepository;
import com.devcamp.s50.provincedistrictwardapi.repository.IWardRepository;

@Service
public class CWardService {
    @Autowired
    IWardRepository wardRepository;
    @Autowired
    IDistrictRepository districtRepository;
    @Autowired
    IProvinceRepository provinceRepository;

    //Get all wards
    public ResponseEntity<List<CWard>> getAllWards(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CWard> wardList = new ArrayList<>();
            wardRepository.findAll(pageWithElements).forEach(wardList::add);

            return new ResponseEntity<>(wardList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get ward by ward id
    public ResponseEntity<Object> getWardById(int id){
        try {
            Optional<CWard> wardData = wardRepository.findById(id);
            if(wardData.isPresent()){
                CWard wardFound = wardData.get();

                return new ResponseEntity<>(wardFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //get ward by district id
    public ResponseEntity<Set<CWard>> getWardByDistrictId(int districtId){
        try {
            Optional<CDistrict> districtData = districtRepository.findById(districtId);
            if(districtData.isPresent()){
                CDistrict districtFound = districtData.get();
                Set<CWard> wardFound = districtFound.getWards();

                return new ResponseEntity<>(wardFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // Create ward by district id and province id
    public ResponseEntity<Object> createWard(int provinceId, int districtId, CWard pWard){
        try {
            Optional<CProvince> provinceData =  provinceRepository.findById(provinceId);
            Optional<CDistrict> districtData = districtRepository.findById(districtId);
            if (provinceData.isPresent() && districtData.isPresent()){
                CProvince provinceFound = provinceData.get();
                CDistrict districtFound = districtData.get();
                CWard newWard = new CWard();
                newWard.setProvince(provinceFound);
                newWard.setPrefix(pWard.getPrefix());
                newWard.setName(pWard.getName());
                newWard.setDistrict(districtFound);
                CWard savedWard = wardRepository.save(newWard);
                return new ResponseEntity<>(savedWard, HttpStatus.CREATED);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified ward: " + e.getCause().getCause().getMessage());
        }
    }
    //Update ward by id
    public ResponseEntity<Object> updateWardById(int provinceId, int districtId,int id, CWard pWard){
        try {
            Optional<CWard> wardData = wardRepository.findById(id);
            Optional<CProvince> provinceData =  provinceRepository.findById(provinceId);
            Optional<CDistrict> districtData = districtRepository.findById(districtId);

            if (wardData.isPresent()){
                CWard wardFound =  wardData.get();
                wardFound.setPrefix(pWard.getPrefix());
                wardFound.setName(pWard.getName());
                wardFound.setDistrict(districtData.get());
                wardFound.setProvince(provinceData.get());

                return new ResponseEntity<>(wardRepository.save(wardFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //delete ward by id
    public ResponseEntity<Object> deleteWardById(int id){
        try {
            Optional<CWard> wardData = wardRepository.findById(id);
            if(wardData.isPresent()){
                wardRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
