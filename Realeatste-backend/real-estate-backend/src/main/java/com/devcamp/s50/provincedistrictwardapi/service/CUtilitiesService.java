package com.devcamp.s50.provincedistrictwardapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.provincedistrictwardapi.model.CUtilities;
import com.devcamp.s50.provincedistrictwardapi.repository.IUtilitiesRepository;

@Service
public class CUtilitiesService {
    @Autowired
    IUtilitiesRepository utilitiesRepository;

    //Get all Utilities
    public ResponseEntity<List<CUtilities>> getAllUtilities(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CUtilities> utilitiesList = new ArrayList<>();

            utilitiesRepository.findAll(pageWithElements).forEach(utilitiesList::add);

            return new ResponseEntity<>(utilitiesList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Get Utitlities By Id
    public ResponseEntity<Object> getUtilitiesById (Integer id){
        try {
            Optional<CUtilities> utilitiesData = utilitiesRepository.findById(id);
            if(utilitiesData.isPresent()){

                CUtilities utilitiesFound = utilitiesData.get();

                return new ResponseEntity<>(utilitiesFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create a new Utilities
    public ResponseEntity<Object> createUtilities(CUtilities pUtilities){
        try {

            CUtilities newUtilities = new CUtilities();

            newUtilities.setName(pUtilities.getName());
            newUtilities.setDescription(pUtilities.getDescription());
            newUtilities.setPhoto(pUtilities.getPhoto());

            CUtilities savedUtilities = utilitiesRepository.save(newUtilities);

            return new ResponseEntity<>(savedUtilities, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified Utitlities: " + e.getCause().getCause().getMessage());
        }
    }

    //Update Design Unit by Design Unit id
    public ResponseEntity<Object> updateUtilitiesById(Integer id, CUtilities pUtilities){
        try {
            Optional<CUtilities> utilitiesData = utilitiesRepository.findById(id);

            if (utilitiesData.isPresent()){

                CUtilities utilitiesFound =  utilitiesData.get();

                utilitiesFound.setName(pUtilities.getName());
                utilitiesFound.setDescription(pUtilities.getDescription());
                utilitiesFound.setProjects(pUtilities.getProjects());
                utilitiesFound.setPhoto(pUtilities.getPhoto());

                return new ResponseEntity<>(utilitiesRepository.save(utilitiesFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete design Unit by id
    public ResponseEntity<Object> deleteUtilitiesById(Integer id){
        try {
            Optional<CUtilities> utilitiesData = utilitiesRepository.findById(id);
            if(utilitiesData.isPresent()){
                utilitiesRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
