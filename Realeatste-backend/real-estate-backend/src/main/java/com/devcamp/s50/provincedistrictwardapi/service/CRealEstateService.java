package com.devcamp.s50.provincedistrictwardapi.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.provincedistrictwardapi.model.CCustomer;
import com.devcamp.s50.provincedistrictwardapi.model.CDistrict;
import com.devcamp.s50.provincedistrictwardapi.model.CProject;
import com.devcamp.s50.provincedistrictwardapi.model.CProvince;
import com.devcamp.s50.provincedistrictwardapi.model.CRealEstate;
import com.devcamp.s50.provincedistrictwardapi.model.CStreet;
import com.devcamp.s50.provincedistrictwardapi.model.CWard;
import com.devcamp.s50.provincedistrictwardapi.repository.ICustomerRepository;
import com.devcamp.s50.provincedistrictwardapi.repository.IDistrictRepository;
import com.devcamp.s50.provincedistrictwardapi.repository.IProjectRepository;
import com.devcamp.s50.provincedistrictwardapi.repository.IProvinceRepository;
import com.devcamp.s50.provincedistrictwardapi.repository.IRealEstateRepository;
import com.devcamp.s50.provincedistrictwardapi.repository.IStreetRepository;
import com.devcamp.s50.provincedistrictwardapi.repository.IWardRepository;

@Service
public class CRealEstateService {
    @Autowired
    IRealEstateRepository realEstateRepository;
    @Autowired
    IProvinceRepository provinceRepository;
    @Autowired
    IDistrictRepository districtRepository;
    @Autowired
    IWardRepository wardRepository;
    @Autowired
    IStreetRepository streetRepository;
    @Autowired
    IProjectRepository projectRepository; 
    @Autowired
    ICustomerRepository customerRepository;

    // Get all Real estates
    public ResponseEntity<List<CRealEstate>> getAllRealEstate(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CRealEstate> realEstateList = new ArrayList<>();
            realEstateRepository.findAll(pageWithElements).forEach(realEstateList::add);

            return new ResponseEntity<>(realEstateList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // get Estates by id
    public ResponseEntity<Object> getDesignUnitById (Integer id){
        try {
            Optional<CRealEstate> realEstateData = realEstateRepository.findById(id);
            if(realEstateData.isPresent()){

                CRealEstate realEstateFound = realEstateData.get();

                return new ResponseEntity<>(realEstateFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Create a realestate
    public ResponseEntity<Object> createRealEstate(CRealEstate pRealEstate,Integer customerId, Integer provinceId, Integer districtId, Integer wardId,
     Integer streetId, Integer projectId){
        try {

            Optional<CProvince> provinceData = provinceRepository.findById(provinceId);
            Optional<CDistrict> districtData = districtRepository.findById(districtId);
            Optional<CWard> wardData = wardRepository.findById(wardId);
            Optional<CStreet> streetData = streetRepository.findById(streetId);
            Optional<CProject> projectData = projectRepository.findById(projectId);
            Optional<CCustomer> customerData = customerRepository.findById(customerId);
            
            CRealEstate newRealEstate = new CRealEstate();

            newRealEstate.setAddress(pRealEstate.getAddress());
            newRealEstate.setProvince(provinceData.get());
            newRealEstate.setDistrict(districtData.get());
            newRealEstate.setWard(wardData.get());
            newRealEstate.setStreet(streetData.get());
            newRealEstate.setProject(projectData.get());
            newRealEstate.setCustomer(customerData.get());
            
            newRealEstate.setTitle(pRealEstate.getTitle());
            newRealEstate.setType(pRealEstate.getType());
            newRealEstate.setRequest(pRealEstate.getRequest());
            newRealEstate.setPrice(pRealEstate.getPrice());
            newRealEstate.setPriceMin(pRealEstate.getPriceMin());
            newRealEstate.setPriceTime(pRealEstate.getPriceTime());

            newRealEstate.setDate_create(new Date());
            newRealEstate.setAcreage(pRealEstate.getAcreage());
            newRealEstate.setDirection(pRealEstate.getDirection());
            newRealEstate.setTotal_floors(pRealEstate.getTotal_floors());
            newRealEstate.setNumber_floors(pRealEstate.getNumber_floors());
            newRealEstate.setBath(pRealEstate.getBath());
            newRealEstate.setApart_code(pRealEstate.getApart_code());
            newRealEstate.setWall_area(pRealEstate.getWall_area());
            newRealEstate.setBedroom(pRealEstate.getBedroom());
            newRealEstate.setBalcony(pRealEstate.getBalcony());
            newRealEstate.setLandscape_view(pRealEstate.getLandscape_view());
            newRealEstate.setApart_loca(pRealEstate.getApart_loca());
            newRealEstate.setApart_type(pRealEstate.getApart_type());

            newRealEstate.setPrice_rent(pRealEstate.getPrice_rent());
            newRealEstate.setReturn_rate(pRealEstate.getReturn_rate());
            newRealEstate.setLegal_doc(pRealEstate.getLegal_doc());
            newRealEstate.setDescription(pRealEstate.getDescription());
            newRealEstate.setWidth_y(pRealEstate.getWidth_y());
            newRealEstate.setLong_x(pRealEstate.getLong_x());
            newRealEstate.setStreet_house(pRealEstate.getStreet_house());
            newRealEstate.setFSBO(pRealEstate.getFSBO());
            newRealEstate.setView_num(pRealEstate.getView_num());
            newRealEstate.setShape(pRealEstate.getShape());
            newRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
            newRealEstate.setFurniture_type(pRealEstate.getFurniture_type());

            newRealEstate.setAdjacent_facade_num(pRealEstate.getAdjacent_facade_num());
            newRealEstate.setAdjacent_road(pRealEstate.getAdjacent_road());
            newRealEstate.setAlley_min_width(pRealEstate.getAlley_min_width());
            newRealEstate.setAdjacent_alley_min_width(pRealEstate.getAdjacent_alley_min_width());
            newRealEstate.setFactor(pRealEstate.getFactor());
            newRealEstate.setStructure(pRealEstate.getStructure());
            newRealEstate.setDTSXD(pRealEstate.getDTSXD());
            newRealEstate.setCLCL(pRealEstate.getCLCL());
            newRealEstate.setCTXD_price(pRealEstate.getCTXD_price());
            newRealEstate.setCTXD_value(pRealEstate.getCTXD_value());
            newRealEstate.setPhoto(pRealEstate.getPhoto());
            newRealEstate.setLat(pRealEstate.getLat());
            newRealEstate.setLng(pRealEstate.getLng());


            CRealEstate savedRealEstate = realEstateRepository.save(newRealEstate);

            return new ResponseEntity<>(savedRealEstate, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified Realestate: " + e.getCause().getCause().getMessage());
        }
    }
    //Update a realestate by id
    public ResponseEntity<Object> updateRealEstate(Integer id, CRealEstate pRealEstate, Integer customerId, Integer provinceId, Integer districtId, Integer wardId,
     Integer streetId, Integer projectId){
        
        try {
            Optional<CRealEstate> realEstateData = realEstateRepository.findById(id);

            if(realEstateData.isPresent()){

                Optional<CProvince> provinceData = provinceRepository.findById(provinceId);
                Optional<CDistrict> districtData = districtRepository.findById(districtId);
                Optional<CWard> wardData = wardRepository.findById(wardId);
                Optional<CStreet> streetData = streetRepository.findById(streetId);
                Optional<CProject> projectData = projectRepository.findById(projectId);
                Optional<CCustomer> customerData = customerRepository.findById(customerId);
                
                CRealEstate updateRealEstate = realEstateData.get();
    
                updateRealEstate.setAddress(pRealEstate.getAddress());
                updateRealEstate.setProvince(provinceData.get());
                updateRealEstate.setDistrict(districtData.get());
                updateRealEstate.setWard(wardData.get());
                updateRealEstate.setStreet(streetData.get());
                updateRealEstate.setProject(projectData.get());
                updateRealEstate.setCustomer(customerData.get());
    
                updateRealEstate.setTitle(pRealEstate.getTitle());
                updateRealEstate.setType(pRealEstate.getType());
                updateRealEstate.setRequest(pRealEstate.getRequest());
                updateRealEstate.setPrice(pRealEstate.getPrice());
                updateRealEstate.setPriceMin(pRealEstate.getPriceMin());
                updateRealEstate.setPriceTime(pRealEstate.getPriceTime());
    
                updateRealEstate.setDate_create(new Date());
                updateRealEstate.setAcreage(pRealEstate.getAcreage());
                updateRealEstate.setDirection(pRealEstate.getDirection());
                updateRealEstate.setTotal_floors(pRealEstate.getTotal_floors());
                updateRealEstate.setNumber_floors(pRealEstate.getNumber_floors());
                updateRealEstate.setBath(pRealEstate.getBath());
                updateRealEstate.setApart_code(pRealEstate.getApart_code());
                updateRealEstate.setWall_area(pRealEstate.getWall_area());
                updateRealEstate.setBedroom(pRealEstate.getBedroom());
                updateRealEstate.setBalcony(pRealEstate.getBalcony());
                updateRealEstate.setLandscape_view(pRealEstate.getLandscape_view());
                updateRealEstate.setApart_loca(pRealEstate.getApart_loca());
                updateRealEstate.setApart_type(pRealEstate.getApart_type());
    
                updateRealEstate.setPrice_rent(pRealEstate.getPrice_rent());
                updateRealEstate.setReturn_rate(pRealEstate.getReturn_rate());
                updateRealEstate.setLegal_doc(pRealEstate.getLegal_doc());
                updateRealEstate.setDescription(pRealEstate.getDescription());
                updateRealEstate.setWidth_y(pRealEstate.getWidth_y());
                updateRealEstate.setLong_x(pRealEstate.getLong_x());
                updateRealEstate.setStreet_house(pRealEstate.getStreet_house());
                updateRealEstate.setFSBO(pRealEstate.getFSBO());
                updateRealEstate.setView_num(pRealEstate.getView_num());
                updateRealEstate.setShape(pRealEstate.getShape());
                updateRealEstate.setDistance2facade(pRealEstate.getDistance2facade());
                updateRealEstate.setFurniture_type(pRealEstate.getFurniture_type());
  
                updateRealEstate.setAdjacent_facade_num(pRealEstate.getAdjacent_facade_num());
                updateRealEstate.setAdjacent_road(pRealEstate.getAdjacent_road());
                updateRealEstate.setAlley_min_width(pRealEstate.getAlley_min_width());
                updateRealEstate.setAdjacent_alley_min_width(pRealEstate.getAdjacent_alley_min_width());
                updateRealEstate.setFactor(pRealEstate.getFactor());
                updateRealEstate.setStructure(pRealEstate.getStructure());
                updateRealEstate.setDTSXD(pRealEstate.getDTSXD());
                updateRealEstate.setCLCL(pRealEstate.getCLCL());
                updateRealEstate.setCTXD_price(pRealEstate.getCTXD_price());
                updateRealEstate.setCTXD_value(pRealEstate.getCTXD_value());
                updateRealEstate.setPhoto(pRealEstate.getPhoto());
                updateRealEstate.setLat(pRealEstate.getLat());
                updateRealEstate.setLng(pRealEstate.getLng());
    
                CRealEstate savedRealEstate = realEstateRepository.save(updateRealEstate);
    
                return new ResponseEntity<>(savedRealEstate, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Delete realestate by id 
    public ResponseEntity<Object> deleteRealEstateById(Integer id){
        try {
            Optional<CRealEstate> realEstateData = realEstateRepository.findById(id);
            if(realEstateData.isPresent()){
                realEstateRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Find by columns
    // public List<CRealEstate> findByReques(
    //     String request, String provinceId, String price, String acreage){
    //             List<CRealEstate> realEstateData = realEstateRepository.findByReques(
    //                 request, provinceId, price, acreage);
    //         return realEstateData;
    // }
}
