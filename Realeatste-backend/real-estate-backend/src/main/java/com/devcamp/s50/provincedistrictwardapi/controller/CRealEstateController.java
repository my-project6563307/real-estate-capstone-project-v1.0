package com.devcamp.s50.provincedistrictwardapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.provincedistrictwardapi.model.CRealEstate;
import com.devcamp.s50.provincedistrictwardapi.service.CRealEstateService;
import com.devcamp.s50.provincedistrictwardapi.repository.IRealEstateRepository;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CRealEstateController {
    @Autowired
    CRealEstateService realEstateService;
    @Autowired
    IRealEstateRepository realEstateRepository;

    //Get all Real estate
    @GetMapping("real_estates")
    public ResponseEntity<List<CRealEstate>> getAllRealEstate(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return realEstateService.getAllRealEstate(page, size);
    }

    // get Estates by id
    @GetMapping("real_estates/{id}")
    public ResponseEntity<Object> getDesignUnitById (@PathVariable Integer id){
        return realEstateService.getDesignUnitById(id);
    }

    //Create a realestate
    @PostMapping("real_estates")
    public ResponseEntity<Object> createRealEstate(@RequestBody CRealEstate pRealEstate, 
    @RequestParam Integer customerId,
    @RequestParam Integer provinceId,
    @RequestParam Integer districtId,@RequestParam Integer wardId,
    @RequestParam Integer streetId,@RequestParam Integer projectId){
        return realEstateService.createRealEstate(pRealEstate, customerId, provinceId, districtId, wardId, streetId, projectId);
    }
     //Update a realestate by id
     @PutMapping("real_estates/{id}")
    public ResponseEntity<Object> updateRealEstate(@PathVariable Integer id,@RequestBody CRealEstate pRealEstate,
    @RequestParam Integer customerId,@RequestParam Integer provinceId,
    @RequestParam Integer districtId,@RequestParam Integer wardId,
    @RequestParam Integer streetId,@RequestParam Integer projectId){
        return realEstateService.updateRealEstate(id, pRealEstate, customerId, provinceId, districtId, wardId, streetId, projectId);
    }


    //Delete realestate by id 
    @DeleteMapping("real_estates/{id}")
    public ResponseEntity<Object> deleteRealEstateById(@PathVariable Integer id){
        return realEstateService.deleteRealEstateById(id);
    }
    // get Estates by id
    @GetMapping("real_estates/find/request/{request}/province/{provinceId}/price1/{price1}/price2/{price2}/acreage1/{acreage1}/acreage2/{acreage2}")
    public List<CRealEstate> findByReques(@PathVariable String request, @PathVariable String provinceId, 
    @PathVariable String price1,@PathVariable String price2,@PathVariable String acreage1,@PathVariable String acreage2){
            System.out.println(request.equals("-1"));
            if (request.equals("-1")){
                request = "%";
            }
            if(provinceId.equals("-1")){
                provinceId = "%";
            }
            return realEstateRepository.findByRequest(request,provinceId, price1,price2, acreage1, acreage2 );
        }
}
