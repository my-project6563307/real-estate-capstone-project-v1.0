package com.devcamp.s50.provincedistrictwardapi.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.provincedistrictwardapi.model.CProvince;
import com.devcamp.s50.provincedistrictwardapi.repository.IProvinceRepository;

@Service
public class CProvinceService {
    @Autowired
    private IProvinceRepository provinceRepository;
    
    // get all province
    public List<CProvince> getAllProvinces() {
        return provinceRepository.findAll();
    }
    // get a province by id
    public ResponseEntity<Object> getProvinceById(int id) {
        try {
            Optional<CProvince> provinceData = provinceRepository.findById(id);

            if (provinceData.isPresent()) {
                CProvince provinceFound = provinceData.get();
                return new ResponseEntity<>(provinceFound, HttpStatus.OK);

            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // create a province
    public ResponseEntity<Object> createProvince(CProvince pProvince) {
        try {
            CProvince newProvince = new CProvince();
            newProvince.setCode(pProvince.getCode());
            newProvince.setName(pProvince.getName());

            return new ResponseEntity<>(provinceRepository.save(newProvince), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // update a provice by id
    public ResponseEntity<Object> updateProvince(int id, CProvince pProvince) {
        try {
            Optional<CProvince> provinceData = provinceRepository.findById(id);

            if (provinceData.isPresent()) {
                CProvince needUpdateProvince = provinceData.get();
                needUpdateProvince.setCode(pProvince.getCode());
                needUpdateProvince.setName(pProvince.getName());

                return new ResponseEntity<>(provinceRepository.save(needUpdateProvince), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    // delete a province by id
    public ResponseEntity<Object> deleteProvince(int id){
        try {
            Optional<CProvince> provinceData = provinceRepository.findById(id);
            if(provinceData.isPresent()){
                provinceRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            // TODO: handle exception
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
