package com.devcamp.s50.provincedistrictwardapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.provincedistrictwardapi.model.CDesignUnit;

public interface IDesignUnitRepository extends JpaRepository<CDesignUnit, Integer>{
    
}
