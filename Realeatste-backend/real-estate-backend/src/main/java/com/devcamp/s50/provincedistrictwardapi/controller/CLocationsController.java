package com.devcamp.s50.provincedistrictwardapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.provincedistrictwardapi.model.CLocations;
import com.devcamp.s50.provincedistrictwardapi.service.CLocationsService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CLocationsController {
    @Autowired
    CLocationsService locationsService;

    //Get all locations
    @GetMapping("locations")
    public ResponseEntity<List<CLocations>> getAllLocations(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return locationsService.getAllLocations(page, size);
    }

    //Get Location by Location id
    @GetMapping("locations/{id}")
    public ResponseEntity<Object> getLocationById (@PathVariable Integer id){
        return locationsService.getLocationById(id);
    }

    // Create a new Location
    @PostMapping("locations")
    public ResponseEntity<Object> createLocation(@RequestBody CLocations pLocations){
        return locationsService.createLocation(pLocations);
    }

    //Update location by id
    @PutMapping("locations/{id}")
    public ResponseEntity<Object> updateLocationById(@PathVariable Integer id,@RequestBody CLocations pLocations){
        return locationsService.updateLocationById(id, pLocations);
    }

    //delete location by id
    @DeleteMapping("locations/{id}")
    public ResponseEntity<Object> deleteLocationById(@PathVariable Integer id){
        return locationsService.deleteLocationById(id);
    }
}
