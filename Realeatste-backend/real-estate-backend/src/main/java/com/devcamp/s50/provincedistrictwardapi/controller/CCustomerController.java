package com.devcamp.s50.provincedistrictwardapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.provincedistrictwardapi.model.CCustomer;
import com.devcamp.s50.provincedistrictwardapi.service.CCustomerService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CCustomerController {
    @Autowired
    CCustomerService customerService;

    @GetMapping("customers")
    public ResponseEntity<List<CCustomer>> getAllCustomers(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return customerService.getAllCustomers(page, size);
    }

    @GetMapping("customers/{id}")
    public ResponseEntity<Object> getCustomerById (@PathVariable int id){
        return customerService.getCustomerById(id);
    }

    @PostMapping("customers")
    public ResponseEntity<Object> createCustomer(@RequestBody CCustomer pCustomer){
        return customerService.createCustomer(pCustomer);
    }

    @PutMapping("customers/{id}")
    public ResponseEntity<Object> updateteCustomer(@PathVariable int id,@RequestBody CCustomer pCustomer){
        return customerService.updateteCustomer(id, pCustomer);
    }

    @DeleteMapping("customers/{id}")
    public ResponseEntity<Object> deleteCustomerById(@PathVariable int id){
        return customerService.deleteCustomerById(id);
    }
}
