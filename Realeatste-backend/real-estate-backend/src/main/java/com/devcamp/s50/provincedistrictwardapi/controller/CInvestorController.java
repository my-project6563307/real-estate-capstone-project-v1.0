package com.devcamp.s50.provincedistrictwardapi.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.provincedistrictwardapi.model.CInvestor;
import com.devcamp.s50.provincedistrictwardapi.service.CInvestorService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CInvestorController {
    @Autowired
    CInvestorService investorService;
    
    @GetMapping("investors")
    public ResponseEntity<List<CInvestor>> getAllInvestors(
        @RequestParam(defaultValue = "0") String page,
        @RequestParam(defaultValue = "50") String size){
        return investorService.getAllInvestors(page, size);
    }

    @GetMapping("investors/{id}")
    public ResponseEntity<Object> getInvestorById (@PathVariable int id){
        return investorService.getInvestorById(id);
    }

    @PostMapping("investors")
    public ResponseEntity<Object> createInvestor(@RequestBody CInvestor pInvestor){
        return investorService.createInvestor(pInvestor);
    }

    @PutMapping("investors/{id}")
    public ResponseEntity<Object> updateInvestorById(@PathVariable int id,@RequestBody CInvestor pInvestor){
        return investorService.updateInvestorById(id, pInvestor);
    }

    @DeleteMapping("investors/{id}")
    public ResponseEntity<Object> deleteInvestorById(@PathVariable int id){
        return investorService.deleteInvestorById(id);
    }
}
