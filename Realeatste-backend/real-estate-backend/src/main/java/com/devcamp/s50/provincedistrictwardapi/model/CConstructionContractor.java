package com.devcamp.s50.provincedistrictwardapi.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "construction_contractor")
public class CConstructionContractor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotEmpty(message = "Tên nhà thầu không được để trống")
    @Column(name = "name", length = 1000)
    private String name;

    @Column(name = "description", length = 5000)
    private String description;

    @Column(name = "projects", length = 2000)
    private String projects;

    @Column(name = "address")
    private Integer address;

    @Column(name = "phone", length = 50)
    private String phone;

    @Column(name = "phone2", length = 50)
    private String phone2;

    @Column(name = "fax", length = 50)
    private String fax;

    @Column(name = "email", length = 200)
    private String email;

    @Column(name = "website", length = 1000)
    private String website;

    @Column(name = "note")
    private String note;

    @OneToMany(mappedBy = "constructionContractor", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CProject> project;
    // Constructor

    public CConstructionContractor() {
    }

    public CConstructionContractor(Integer id, @NotEmpty(message = "Tên nhà thầu không được để trống") String name,
            String description, String projects, Integer address, String phone, String phone2, String fax, String email,
            String website, String note, Set<CProject> project) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.projects = projects;
        this.address = address;
        this.phone = phone;
        this.phone2 = phone2;
        this.fax = fax;
        this.email = email;
        this.website = website;
        this.note = note;
        this.project = project;
    }
    // Getter and setter

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProjects() {
        return projects;
    }

    public void setProjects(String projects) {
        this.projects = projects;
    }

    public Integer getAddress() {
        return address;
    }

    public void setAddress(Integer address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Set<CProject> getProject() {
        return project;
    }

    public void setProject(Set<CProject> project) {
        this.project = project;
    }

}
