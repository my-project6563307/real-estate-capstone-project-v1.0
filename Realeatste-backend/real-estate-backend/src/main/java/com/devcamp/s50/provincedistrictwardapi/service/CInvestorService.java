package com.devcamp.s50.provincedistrictwardapi.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.s50.provincedistrictwardapi.model.CInvestor;
import com.devcamp.s50.provincedistrictwardapi.repository.IInvestorRepository;

@Service
public class CInvestorService {
    @Autowired
    IInvestorRepository investorRepository;

    //Get all Investors
    public ResponseEntity<List<CInvestor>> getAllInvestors(String page, String size){
        try {
            Pageable pageWithElements = PageRequest.of(Integer.parseInt(page), Integer.parseInt(size));

            List<CInvestor> investorList = new ArrayList<>();
            investorRepository.findAll(pageWithElements).forEach(investorList::add);

            return new ResponseEntity<>(investorList, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    //Get Investor by investor id
    public ResponseEntity<Object> getInvestorById (int id){
        try {
            Optional<CInvestor> investorData = investorRepository.findById(id);
            if(investorData.isPresent()){

                CInvestor investorFound = investorData.get();

                return new ResponseEntity<>(investorFound, HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Create a new investor
    public ResponseEntity<Object> createInvestor(CInvestor pInvestor){
        try {

            CInvestor newInvestor = new CInvestor();

            newInvestor.setName(pInvestor.getName());
            newInvestor.setDescription(pInvestor.getDescription());
            newInvestor.setProjects(pInvestor.getProjects());
            newInvestor.setAddress(pInvestor.getAddress());
            newInvestor.setPhone(pInvestor.getPhone());
            newInvestor.setPhone2(pInvestor.getPhone2());
            newInvestor.setFax(pInvestor.getFax());
            newInvestor.setEmail(pInvestor.getEmail());
            newInvestor.setWebsite(pInvestor.getWebsite());
            newInvestor.setNote(pInvestor.getNote());

            CInvestor savedInvestor = investorRepository.save(newInvestor);

            return new ResponseEntity<>(savedInvestor, HttpStatus.CREATED);

        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Create a specified Investor: " + e.getCause().getCause().getMessage());
        }
    }

    //Update Investor by id
    public ResponseEntity<Object> updateInvestorById(int id, CInvestor pInvestor){
        try {
            Optional<CInvestor> investorData = investorRepository.findById(id);

            if (investorData.isPresent()){

                CInvestor investorFound =  investorData.get();

                investorFound.setName(pInvestor.getName());
                investorFound.setDescription(pInvestor.getDescription());
                investorFound.setProjects(pInvestor.getProjects());
                investorFound.setAddress(pInvestor.getAddress());
                investorFound.setPhone(pInvestor.getPhone());
                investorFound.setPhone2(pInvestor.getPhone2());
                investorFound.setFax(pInvestor.getFax());
                investorFound.setEmail(pInvestor.getEmail());
                investorFound.setWebsite(pInvestor.getWebsite());
                investorFound.setNote(pInvestor.getNote());

                return new ResponseEntity<>(investorRepository.save(investorFound), HttpStatus.OK);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    //delete investor by id
    public ResponseEntity<Object> deleteInvestorById(int id){
        try {
            Optional<CInvestor> investorData = investorRepository.findById(id);
            if(investorData.isPresent()){
                investorRepository.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
