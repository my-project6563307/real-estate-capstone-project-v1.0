package com.devcamp.s50.provincedistrictwardapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.devcamp.s50.provincedistrictwardapi.model.CWard;

@Service
public interface IWardRepository extends JpaRepository<CWard, Integer> {

}
