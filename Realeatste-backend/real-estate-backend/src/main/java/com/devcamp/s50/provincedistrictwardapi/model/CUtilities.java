package com.devcamp.s50.provincedistrictwardapi.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "utilities")
public class CUtilities {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name", length = 1000)
    private String name;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "photo", length = 5000)
    private String photo;

    @OneToMany(mappedBy = "utilities", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<CProject> projects;
    // constructor

    public CUtilities() {
    }

    public CUtilities(Integer id, String name, String description, String photo, Set<CProject> projects) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.photo = photo;
        this.projects = projects;
    }
    // getter and setter

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Set<CProject> getProjects() {
        return projects;
    }

    public void setProjects(Set<CProject> projects) {
        this.projects = projects;
    }

}
